FROM debian:jessie

MAINTAINER Adam Sanchez <a.sanchez75@gmail.com>

# Install Apache 2.x and PHP 5.6
RUN apt-get update && apt-get install -y apache2 \
      apache2-dev \
      php5 \
      php5-curl \
      php5-dev \
      php5-odbc \
      libapache2-mod-php5 \
      vim \
      mlocate \
      git \
      unixodbc \
      net-tools \
      && a2dismod mpm_event \
      && a2enmod mpm_prefork

ADD odbc.ini /etc/odbc.ini
ADD odbcinst.ini /etc/odbcinst.ini
ADD info.php /var/www/html/info.php
ADD connection.php /var/www/html/connection.php

# composer
RUN cd /usr/local/bin && curl -sS https://getcomposer.org/installer | php -- --filename=composer

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
